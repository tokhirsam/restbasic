package com.epam.esm.service;

import com.epam.esm.model.Employee;
import com.epam.esm.model.GiftCertificate;

import java.util.List;

public interface CertificateService {
//    Employee getEmployeeById(String id);
//    List<Employee> getEmployees();
    GiftCertificate getCertificateById(Long id);
}
