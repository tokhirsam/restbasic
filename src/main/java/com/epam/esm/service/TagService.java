package com.epam.esm.service;

import com.epam.esm.model.GiftCertificate;
import com.epam.esm.model.Tag;

import java.util.List;

public interface TagService {
//    Employee getEmployeeById(String id);
//    List<Employee> getEmployees();
    Tag getTagById(Long id);
    List <Tag> getAllTags();
}
