package com.epam.esm.service.impl;

import com.epam.esm.dao.TagDao;
import com.epam.esm.model.Tag;
import com.epam.esm.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagServiceImpl implements TagService {

    @Autowired
    private TagDao tagDao;

//    @Override
//    public Employee getEmployeeById(String id) {
//        return employeeDao.findById(id);
//    }
//
//    @Override
//    public List<Employee> getEmployees() {
//        return employeeDao.findAll();
//    }

//    @Override
//    public GiftCertificate getCertificateById(Long id) {
//        return employeeDao.getCertificateById(id);
//    }

    @Override
    public Tag getTagById(Long id) {
        return tagDao.getTagById(id);
    }

    @Override
    public List<Tag> getAllTags() {
        return tagDao.findAll();
    }
}
