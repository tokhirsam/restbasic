package com.epam.esm.service.impl;

import com.epam.esm.dao.CertificateDao;
import com.epam.esm.model.GiftCertificate;
import com.epam.esm.service.CertificateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CertificateServiceImpl implements CertificateService {

    @Autowired
    private CertificateDao employeeDao;

//    @Override
//    public Employee getEmployeeById(String id) {
//        return employeeDao.findById(id);
//    }
//
//    @Override
//    public List<Employee> getEmployees() {
//        return employeeDao.findAll();
//    }

    @Override
    public GiftCertificate getCertificateById(Long id) {
        return employeeDao.getCertificateById(id);
    }
}
