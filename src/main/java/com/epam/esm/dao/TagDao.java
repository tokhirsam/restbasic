package com.epam.esm.dao;

import com.epam.esm.model.GiftCertificate;
import com.epam.esm.model.Tag;

import java.util.List;

public interface TagDao {
//    Employee findById(String id);
    List<Tag> findAll();
    Tag getTagById(Long id);
}
