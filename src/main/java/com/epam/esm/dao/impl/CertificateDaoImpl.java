package com.epam.esm.dao.impl;

import com.epam.esm.dao.CertificateDao;
import com.epam.esm.dao.mappers.GiftCertificateMapper;
import com.epam.esm.model.GiftCertificate;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class CertificateDaoImpl implements CertificateDao {

    private final JdbcTemplate jdbcTemplate;

    private static final String FIND_BY_ID_SQL = "select * from certificate where id = ?";

//    @Override
//    public Employee findById(String id) {
//        return jdbcTemplate.queryForObject(FIND_BY_ID_SQL, new Object[]{id}, new EmployeeMapper());
//    }

    @Override
    public GiftCertificate getCertificateById(Long id) {
        return jdbcTemplate.queryForObject(FIND_BY_ID_SQL,
                new Object[]{id}, new GiftCertificateMapper());
    }
//    @Override
//    public List<Employee> findAll() {
//        return null;
//    }
}
