package com.epam.esm.dao.impl;

import com.epam.esm.dao.TagDao;
import com.epam.esm.dao.mappers.TagMapper;
import com.epam.esm.model.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class TagDaoImpl implements TagDao {

    private final JdbcTemplate jdbcTemplate;

    private final String SQL_FIND_TAG = "select * from tag where id = ?";
    private final String SQL_DELETE_TAG = "delete from tag where id = ?";
    private final String SQL_UPDATE_TAG = "update tag set name = ? where id = ?";
    private final String SQL_GET_ALL = "select * from tag";
    private final String SQL_INSERT_TAG = "insert into tag(name) values(?)";

    @Override
    public List<Tag> findAll() {
        return jdbcTemplate.query(SQL_GET_ALL, new TagMapper());
    }

    @Override
    public Tag getTagById(Long id) {
        return jdbcTemplate.queryForObject(SQL_FIND_TAG,
                new Object[]{id}, new TagMapper());
    }
}
