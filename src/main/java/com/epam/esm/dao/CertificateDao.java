package com.epam.esm.dao;

import com.epam.esm.model.Employee;
import com.epam.esm.model.GiftCertificate;

import java.util.List;

public interface CertificateDao {
//    Employee findById(String id);
//    List<Employee> findAll();
    GiftCertificate getCertificateById(Long id);
}
