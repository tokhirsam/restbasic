package com.epam.esm.controller;

import com.epam.esm.model.Employee;
import com.epam.esm.model.GiftCertificate;
import com.epam.esm.model.Tag;
import com.epam.esm.service.CertificateService;
import com.epam.esm.service.TagService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping("/api/tag")
public class TagController {

    @Autowired
    private TagService tagService;

    @GetMapping("/{id}")
    public HttpEntity<?> getOneById(@PathVariable Long id) {
        Tag oneById = tagService.getTagById(id);
        return ResponseEntity.ok(oneById);
    }

    @GetMapping
    public HttpEntity<?> getAll() {

        List<Tag> oneById = tagService.getAllTags();
        return ResponseEntity.ok(oneById);
    }
}
