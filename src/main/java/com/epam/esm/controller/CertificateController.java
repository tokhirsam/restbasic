package com.epam.esm.controller;

import com.epam.esm.model.Employee;
import com.epam.esm.model.GiftCertificate;
import com.epam.esm.service.CertificateService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class CertificateController {

    @Autowired
    private CertificateService employeeService;

    @RequestMapping("/")
    public String display() {
        return "index";
    }
    Employee employee = new Employee();

        @GetMapping("/employee")
        @ResponseBody
        public ResponseEntity<?> getEmployee() {
            GiftCertificate employee = employeeService.getCertificateById(1L);
            return ResponseEntity.ok(employee);

        }
}
